FROM registry.lendfoundry.com/base:beta8

ADD ./global.json /app/

ADD ./src/LendFoundry.Migration.Loans.V3 /app/src/LendFoundry.Migration.Loans.V3
WORKDIR /app/src/LendFoundry.Migration.Loans.V3
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel

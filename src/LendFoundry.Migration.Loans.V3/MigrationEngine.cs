﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans;
using LendFoundry.Migration.Loans.V3.Interest;
using LendFoundry.Migration.Loans.V3.NewVersion;
using LendFoundry.Migration.Loans.V3.OldVersion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3
{
    public class MigrationEngine
    {
        public MigrationEngine(
            LoanRepository loanRepo,
            OldPaymentScheduleRepository oldRepo,
            NewPaymentScheduleRepository newRepo,
            InterestCalculator interestCalculator,
            MigrationConfiguration configuration,
            ITenantTime tenantTime,
            ILogger logger
        )
        {
            LoanRepo = loanRepo;
            OldRepo = oldRepo;
            NewRepo = newRepo;
            InterestCalculator = interestCalculator;
            Configuration = configuration;
            TenantTime = tenantTime;
            Logger = logger;

            RoundingModeChangedDate = TenantTime.Create(2016, 4, 1);
        }

        private LoanRepository LoanRepo { get; }
        private OldPaymentScheduleRepository OldRepo { get; }
        private NewPaymentScheduleRepository NewRepo { get; }
        private InterestCalculator InterestCalculator { get; }
        private MigrationConfiguration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }

        private DateTimeOffset RoundingModeChangedDate { get; }

        public MigrationErrors Run()
        {
            var refnums = OldRepo.GetRefNums();

            Logger.Info("Processing {Count} loans", new { refnums.Count });

            var allErrors = new Dictionary<string, IEnumerable<string>>();

            foreach (var refnum in refnums)
            {
                var errors = Run(refnum);

                if (errors.Any())
                {
                    allErrors[refnum] = errors;
                }
            }

            Logger.Info("Finished processing {Count} loans", new { refnums.Count });

            return new MigrationErrors(allErrors, Configuration);
        }

        public IEnumerable<string> Run(string refnum)
        {
            Logger.Info("Processing loan {refnum}", new { refnum });

            var loan = GetLoan(refnum);
            var oldSchedule = GetOldSchedule(refnum);
            var newInstallments = new List<IInstallment>();

            var newSchedule = new PaymentSchedule
            {
                Id = oldSchedule.Id,
                TenantId = oldSchedule.TenantId,
                LoanReferenceNumber = oldSchedule.LoanReferenceNumber,
                Installments = newInstallments
            };

            foreach (var oldInstallment in oldSchedule.Installments)
            {
                DateTimeOffset dueDate = oldInstallment.DueDate.Value;
                DateTimeOffset? graceDate = oldInstallment.Type == OldInstallmentType.Scheduled ? dueDate.AddDays(GracePeriod.InDays) : null as DateTimeOffset?;

                var newInstallment = new Installment
                {
                    AnniversaryDate = oldInstallment.AnniversaryDate,
                    DueDate = oldInstallment.DueDate,
                    GraceDate = graceDate,
                    IsLate = graceDate != null && TenantTime.Today > graceDate,
                    OpeningBalance = oldInstallment.OpeningBalance,
                    PaymentDate = oldInstallment.PaymentDate,
                    PaymentId = oldInstallment.PaymentId,
                    PaymentMethod = oldInstallment.PaymentMethod,
                    Status = ToNewStatus(oldInstallment, graceDate),
                    Type = ToNewType(oldInstallment.Type)
                };

                newInstallment.SetExpectedAmount(GetExpectedAmount(oldInstallment, loan));

                newInstallment.SetExpectedInterest(
                    ShouldRecalculateInterest(oldSchedule)
                        ? GetExpectedInterest(newInstallment, newSchedule, loan)
                        : oldInstallment.Interest);

                if (newInstallment.IsPaid)
                {
                    newInstallment.SetActualAmount(oldInstallment.PaymentAmount);
                }
                else if (newInstallment.Status == InstallmentStatus.Unpaid)
                {
                    newInstallment.SetActualAmount(0.0);
                }

                newInstallments.Add(newInstallment);
            }

            var errors = Validate(newSchedule, oldSchedule, loan.Terms);

            NewRepo.Upsert(newSchedule);

            return errors;
        }

        private bool ShouldRecalculateInterest(IOldPaymentSchedule oldSchedule)
        {
            return !IsPaidOff(oldSchedule) && HasUnpaidInstallments(oldSchedule);
        }

        private bool HasUnpaidInstallments(IOldPaymentSchedule oldSchedule)
        {
            return oldSchedule.Installments.Any(i => i.Type == OldInstallmentType.Scheduled && i.PaymentAmount == 0.0);
        }

        private bool IsPaidOff(IOldPaymentSchedule oldSchedule)
        {
            return oldSchedule.Installments.All(i => i.Status == OldInstallmentStatus.Completed);
        }

        public MigrationErrors ValidateAll()
        {
            var refnums = OldRepo.GetRefNums();

            Logger.Info("Validating {Count} payment schedules", new { refnums.Count });

            var allErrors = new Dictionary<string, IEnumerable<string>>();

            foreach (var refnum in refnums)
            {
                var errors = Validate(refnum);

                if (errors.Any())
                {
                    allErrors[refnum] = errors;
                }
            }

            Logger.Info("{Count} payment schedules have been validated. Found {InvalidCount} schedules with errors.", new { refnums.Count, InvalidCount = allErrors.Count });

            return new MigrationErrors(allErrors, Configuration);
        }

        public IEnumerable<string> Validate(string refnum)
        {
            var loan = GetLoan(refnum);
            var oldSchedule = GetOldSchedule(refnum);
            var newSchedule = NewRepo.GetSchedule(refnum);

            return Validate(newSchedule, oldSchedule, loan.Terms);
        }

        private IEnumerable<string> Validate(IPaymentSchedule newSchedule, IOldPaymentSchedule oldSchedule, ILoanTerms loanTerms)
        {
            Logger.Info("Validating loan {LoanReferenceNumber}", new { newSchedule.LoanReferenceNumber });

            var errors = new List<string>();

            var oldInstallments = oldSchedule.Installments.ToList();
            var newInstallments = newSchedule.Installments.ToList();

            if (newInstallments.Count != oldInstallments.Count)
                errors.Add(ErrorMessage("inconsistency", $"installment count mismatch: expected {oldInstallments.Count}, got {newInstallments.Count}"));

            var firstInstallment = newInstallments[0];

            if (!SameAmount(firstInstallment.OpeningBalance, loanTerms.LoanAmount))
                errors.Add(ErrorMessage("inconsistency", 0, $"opening balance doesn't match the loan amount: {firstInstallment.OpeningBalance} != {loanTerms.LoanAmount}"));

            for (var index = 1; index < newInstallments.Count; index++)
            {
                var prev = newInstallments[index - 1];
                var curr = newInstallments[index];

                if (!SameAmount(curr.OpeningBalance, prev.EndingBalance))
                    errors.Add(ErrorMessage(IsRoundOffError(curr.OpeningBalance, prev.EndingBalance) ? "round-off" : "inconsistency", index, $"opening balance doesn't match previous ending balance: {curr.OpeningBalance} != {prev.EndingBalance}"));
            }

            for (var index = 0; index < newInstallments.Count; index++)
            {
                var oldInst = oldInstallments[index];
                var newInst = newInstallments[index];

                if (!SameAmount(newInst.OpeningBalance, oldInst.OpeningBalance))
                    errors.Add(ErrorMessage(IsRoundOffError(newInst.OpeningBalance, oldInst.OpeningBalance) ? "round-off" : "inconsistency", index, $"opening balance doesn't match old schedule: {newInst.OpeningBalance} != {oldInst.OpeningBalance}"));

                if (!SameAmount(newInst.EndingBalance, oldInst.EndingBalance))
                    errors.Add(ErrorMessage(IsRoundOffError(newInst.EndingBalance, oldInst.EndingBalance) ? "round-off" : "inconsistency", index, $"ending balance doesn't match old schedule: {newInst.EndingBalance} != {oldInst.EndingBalance}"));

                if (newInst.Type != ToNewType(oldInst.Type))
                    errors.Add(ErrorMessage("inconsistency", index, $"installment type doesn't match old schedule: {newInst.Type} != {oldInst.Type}"));

                var interest = (newInst.Expected.Interest - newInst.InterestBalance);
                if (!SameAmount(interest, oldInst.Interest))
                    errors.Add(ErrorMessage(IsRoundOffError(interest, oldInst.Interest) ? "round-off" : "inconsistency", index, $"interest doesn't match old schedule: {newInst.Expected.Interest} != {oldInst.Interest}"));
            }

            if (errors.Any())
                Logger.Info("Loan {LoanReferenceNumber} has {ErrorCount} error(s)", new { newSchedule.LoanReferenceNumber, ErrorCount = errors.Count });

            return errors;
        }

        private bool IsRoundOffError(double expected, double actual)
        {
            return AreEqual(Math.Abs(expected - actual), 0.01);
        }

        private string ErrorMessage(string type, string msg)
        {
            return $"{{{type}}}: {msg}";
        }

        private string ErrorMessage(string type, int installmentIndex, string msg)
        {
            return $"{{{type}}}: installment #{installmentIndex + 1}: {msg}";
        }

        private bool SameAmount(double a, double b)
        {
            return AreEqual(Math.Round(a, 2), Math.Round(b, 2));
        }

        private bool AreEqual(double a, double b)
        {
            return Math.Abs(a - b) < 0.00001;
        }

        private double GetExpectedInterest(IInstallment installment, IPaymentSchedule partialSchedule, ILoan loan)
        {
            var installments = partialSchedule.Installments.ToList();
            var previousInstallment = installments.LastOrDefault();
            var interestBalance = previousInstallment?.InterestBalance ?? 0.0;

            if (installment.Type == InstallmentType.Extra)
            {
                return interestBalance;
            }

            if (IsRegularPayment(installment) && (previousInstallment == null || IsRegularPayment(previousInstallment)))
            {
                var monthlyInterestRate = loan.Terms.Rate / 100.0 / 12.0;
                var interest = installment.OpeningBalance * monthlyInterestRate;
                return Round(interest + interestBalance, installment, partialSchedule, loan.Terms);
            }

            var calculatedInterest = InterestCalculator.GetInterestOwedFor(installment, partialSchedule, loan);
            return Round(calculatedInterest, installment, partialSchedule, loan.Terms);
        }

        private bool IsRegularPayment(IInstallment installment)
        {
            return installment.Type == InstallmentType.Installment
                && !installment.IsEarlyPayment
                && !installment.IsLatePayment
                && (installment.Actual == null || installment.Actual.Amount > 0.0);
        }

        private double GetExpectedAmount(IOldInstallment oldInstallment, ILoan loan)
        {
            if (oldInstallment.Type == OldInstallmentType.Scheduled && oldInstallment.PaymentAmount == 0.0)
            {
                return loan.Terms.PaymentAmount;
            }

            return oldInstallment.PaymentAmount;
        }

        private InstallmentType ToNewType(OldInstallmentType type)
        {
            if (type == OldInstallmentType.Scheduled) return InstallmentType.Installment;
            if (type == OldInstallmentType.Additional) return InstallmentType.Extra;
            if (type == OldInstallmentType.Payoff) return InstallmentType.Payoff;
            if (type == OldInstallmentType.ChargeOff) return InstallmentType.ChargeOff;
            throw new ArgumentException("Invalid installment type: " + type);
        }

        private InstallmentStatus ToNewStatus(IOldInstallment i, DateTimeOffset? graceDate)
        {
            var status = i.Status;

            if (status == OldInstallmentStatus.Scheduled)
            {
                if (TenantTime.Today > i.DueDate)
                    return InstallmentStatus.PastDue;

                return InstallmentStatus.Open;
            }

            if (status == OldInstallmentStatus.Completed)
            {
                if (i.Type == OldInstallmentType.Scheduled)
                {
                    if (i.PaymentAmount == 0.0)
                        return InstallmentStatus.Unpaid;

                    if (i.PaymentDate < i.AnniversaryDate)
                        return InstallmentStatus.PaidEarly;

                    if (i.PaymentDate > graceDate)
                        return InstallmentStatus.PaidLate;
                }

                return InstallmentStatus.Paid;
            }

            throw new ArgumentException("Invalid installment status: " + status);
        }

        private double Round(double x, IInstallment installment, IPaymentSchedule partialSchedule, ILoanTerms loanTerms)
        {
            var mode = GetRoundingMode(installment, partialSchedule, loanTerms);
            return Math.Round(x, 2, mode);
        }

        private MidpointRounding GetRoundingMode(IInstallment installment, IPaymentSchedule partialSchedule, ILoanTerms loanTerms)
        {
            if (loanTerms.OriginationDate > RoundingModeChangedDate)
                return MidpointRounding.AwayFromZero;

            var installmentDate = (installment.AnniversaryDate ?? installment.DueDate);

            if (installmentDate <= RoundingModeChangedDate)
                return MidpointRounding.ToEven;

            var hasScheduleBeenReamortizedBeforeRoundingModeChanged = partialSchedule.Installments
                .Where(i => i.PrincipalPaymentDate.Time <= RoundingModeChangedDate)
                .Any(i => !IsRegularPayment(i));

            if (hasScheduleBeenReamortizedBeforeRoundingModeChanged)
                return MidpointRounding.AwayFromZero;
            else
                return MidpointRounding.ToEven;
        }

        private ILoan GetLoan(string refnum)
        {
            var loan = LoanRepo.Get(refnum);
            if (loan == null)
                throw new NotFoundException($"Loan not found: {refnum}");
            return loan;
        }

        private IOldPaymentSchedule GetOldSchedule(string refnum)
        {
            var oldSchedule = OldRepo.GetSchedule(refnum);
            if (oldSchedule == null)
                throw new NotFoundException($"Schedule not found: {refnum}");
            return oldSchedule;
        }
    }
}

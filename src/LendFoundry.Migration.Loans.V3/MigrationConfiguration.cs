﻿using System.Collections.Generic;

namespace LendFoundry.Migration.Loans.V3
{
    public class MigrationConfiguration
    {
        public KnownIssue[] KnownIssues { get; set; }
    }

    public class KnownIssue
    {
        public string ReferenceNumber { get; set; }
        public string Description { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}

﻿using LendFoundry.Configuration.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Migration.Loans.V3.Interest;
using LendFoundry.Migration.Loans.V3.NewVersion;
using LendFoundry.Migration.Loans.V3.OldVersion;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Migration.Loans.V3.Principal;

namespace LendFoundry.Migration.Loans.V3
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService(Settings.TenantService.Host, Settings.TenantService.Port);
            services.AddCors();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddConfigurationService<MigrationConfiguration>(Settings.ConfigurationService.Host, Settings.ConfigurationService.Port, "loan-migration-v3");
            services.AddCalendarService(Settings.CalendarService.Host, Settings.CalendarService.Port);
            services.AddTransient(p => p.GetService<IConfigurationServiceFactory<MigrationConfiguration>>().Create(p.GetService<ITokenReader>()).Get());
            services.AddTransient(p => new OldPaymentScheduleRepository(new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database), p.GetService<ITenantService>()));
            services.AddTransient(p => new NewPaymentScheduleRepository(new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database), p.GetService<ITenantService>()));
            services.AddTransient(p => new LoanRepository(new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database), p.GetService<ITenantService>()));
            services.AddTransient<PrincipalBalanceCalculator>();
            services.AddTransient<MigrationEngine>();
            services.AddTransient<LoanCalendarFactory>();
            services.AddTransient<InterestCalculator>();
            services.AddTransient<InterestAccrualDurationCalculator>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }
    }
}

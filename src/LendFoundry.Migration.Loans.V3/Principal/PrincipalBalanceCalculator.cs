﻿using LendFoundry.Loans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Migration.Loans.V3.Principal
{
    public class PrincipalBalanceCalculator
    {
        public IEnumerable<PrincipalBalance> SinceLastInterestPaymentBefore(DateTimeOffset endDate, IPaymentSchedule schedule, ILoan loan)
        {
            var interestPayments = schedule.Installments
                .Where(i => i.IsInterestOwed)
                .Where(i => i.PrincipalPaymentDate.Time < endDate);

            var lastInterestPayment = interestPayments.LastOrDefault();

            var startDate = GetStartDate(lastInterestPayment, loan);

            var principalPayments = schedule.Installments
                .Where(i => i.PrincipalPaymentDate >= startDate)
                .Where(i => i.PrincipalPaymentDate < endDate);

            var history = new List<PrincipalBalance>();

            /* The origination may be the first principal balance occurrence */

            if (startDate == loan.Terms.OriginationDate)
            {
                history.Add(new PrincipalBalance
                {
                    Date = startDate,
                    Amount = loan.Terms.LoanAmount
                });
            }

            /* Add opening balance at the anniversary date for late payments */

            var latePayments = principalPayments.Where(i => i.IsLatePayment);

            history.AddRange(latePayments.Select(i => new PrincipalBalance
            {
                Date = i.InterestPaymentDate,
                Amount = GetOpeningBalanceOn(i.InterestPaymentDate.Time, principalPayments)
            }));

            /* Add ending balances of all principal payments, including late payments */

            history.AddRange(principalPayments.Select(i => new PrincipalBalance
            {
                Date = i.PrincipalPaymentDate,
                Amount = i.EndingBalance
            }));

            return history
                .GroupBy(pb => pb.Date)
                .Select(e => e.OrderBy(pb => pb.Amount).First())
                .OrderBy(pb => pb.Date);
        }

        //private IEnumerable<IInstallment> MergePaymentsOnTheSameDate(IEnumerable<IInstallment> installments)
        //{
        //    return installments.GroupBy(i => i.);
        //}

        private static Date GetStartDate(IInstallment lastInterestPayment, ILoan loan)
        {
            if (lastInterestPayment == null)
                return new Date(loan.Terms.OriginationDate);

            if (lastInterestPayment.IsLatePayment)
                return lastInterestPayment.InterestPaymentDate;

            return lastInterestPayment.PrincipalPaymentDate;
        }

        private double GetOpeningBalanceOn(DateTimeOffset date, IEnumerable<IInstallment> installments)
        {
            var installment = installments.First(i => i.PrincipalPaymentDate.Time >= date);
            return installment.OpeningBalance;
        }

        public double GetPrincipalBalanceOn(DateTimeOffset date, IPaymentSchedule schedule, ILoan loan)
        {
            var installment = schedule.Installments
                .Where(i => i.PrincipalPaymentDate <= date)
                .OrderBy(i => i.PrincipalPaymentDate.Time)
                .ThenByDescending(i => i.EndingBalance)
                .LastOrDefault();

            return installment?.EndingBalance ?? loan.Terms.LoanAmount;
        }
    }
}

﻿using LendFoundry.Loans;

namespace LendFoundry.Migration.Loans.V3.Principal
{
    public class PrincipalBalance
    {
        public PrincipalBalance()
        {

        }

        public PrincipalBalance(double amount, Date date)
        {
            Amount = amount;
            Date = date;
        }

        public double Amount { get; set; }
        public Date Date { get; set; }
    }
}

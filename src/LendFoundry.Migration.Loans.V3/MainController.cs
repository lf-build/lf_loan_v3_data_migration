﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System;

namespace LendFoundry.Migration.Loans.V3
{
    public class MainController : Controller
    {
        public MainController(MigrationEngine engine, ILogger logger)
        {
            Engine = engine;
            Logger = logger;
        }

        private MigrationEngine Engine { get; }
        private ILogger Logger { get; }

        [HttpPost("run")]
        public IActionResult Run()
        {
            return Execute(() =>
            {
                var allErrors = Engine.Run();
                return new HttpOkObjectResult(allErrors);
            });
        }

        [HttpPost("run/{refnum}")]
        public IActionResult Run(string refnum)
        {
            return Execute(() =>
            {
                var errors = Engine.Run(refnum);
                return new HttpOkObjectResult(errors);
            });
        }

        [HttpGet("validate")]
        public IActionResult ValidateAll()
        {
            return Execute(() =>
            {
                var errors = Engine.ValidateAll();
                return new HttpOkObjectResult(errors);
            });
        }

        [HttpGet("validate/{refnum}")]
        public IActionResult Validate(string refnum)
        {
            return Execute(() =>
            {
                var errors = Engine.Validate(refnum);
                return new HttpOkObjectResult(errors);
            });
        }

        private IActionResult Execute(Func<IActionResult> expression)
        {
            try { return expression(); }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                Logger.Error("Error when handling request", ex);
                return ErrorResult.InternalServerError("We couldn't process your request");
            }
        }
    }
}
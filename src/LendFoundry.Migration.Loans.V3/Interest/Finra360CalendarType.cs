﻿using LendFoundry.Loans;
using System;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class Finra360CalendarType
    {
        private const int DaysInTheYear = 360;

        public int GetDaysInTheYear(int year)
        {
            return DaysInTheYear;
        }

        public int DaysBetween(Date startDate, Date endDate, DateTimeOffset origination)
        {
            var start = startDate.Time;
            var end = endDate.Time;

            return (end.Year - start.Year) * 360 +
                   (end.Month - start.Month) * 30 +
                    DayDiff(startDate, endDate, origination);
        }

        private int DayDiff(Date startDate, Date endDate, DateTimeOffset origination)
        {
            var startDay = GetDay(startDate, origination);
            var endDay = GetDay(endDate, origination);

            var start = Math.Min(30, startDay);

            var canEndDateGoBeyondThe30th =
                start < 30 &&
                !LastOfFebruary(startDate) &&
                DoesMonthHaveMoreThan30Days(endDate);

            var end = canEndDateGoBeyondThe30th ? endDay : Math.Min(30, endDay);

            return end - start;
        }

        private static bool DoesMonthHaveMoreThan30Days(Date date)
        {
            DateTimeOffset dateTime = date.Time;
            return DateTime.DaysInMonth(dateTime.Year, dateTime.Month) > 30;
        }

        private int GetDay(Date date, DateTimeOffset origination)
        {
            return date.IsAnniversary ? origination.Day : date.Time.Day;
        }

        private static bool LastOfFebruary(Date date)
        {
            return LastOfFebruary(date.Time);
        }
        
        private static bool LastOfFebruary(DateTimeOffset date)
        {
            if (date.Month != 2)
                return false;

            var lastDay = DateTime.DaysInMonth(date.Year, date.Month);

            return date.Day == lastDay;
        }
    }
}

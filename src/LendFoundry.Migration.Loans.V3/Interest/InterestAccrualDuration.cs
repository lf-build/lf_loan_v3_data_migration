﻿namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class InterestAccrualDuration
    {
        public double Principal { get; set; }
        public int Days { get; set; }
        public int DaysInTheYear { get; set; }
    }
}

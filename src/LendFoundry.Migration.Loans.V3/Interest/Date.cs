﻿using System;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    //public struct Date
    //{
    //    private DateTimeOffset dateTimeOffset;

    //    public Date(DateTimeOffset dateTimeOffset)
    //    {
    //        this.dateTimeOffset = dateTimeOffset;
    //        IsAnniversary = false;
    //    }

    //    public Date(DateTimeOffset dateTimeOffset, bool isAnniversary)
    //    {
    //        this.dateTimeOffset = dateTimeOffset;
    //        IsAnniversary = isAnniversary;
    //    }

    //    public static Date Anniversary(DateTimeOffset dateTime)
    //    {
    //        return new Date(dateTime, true);
    //    }

    //    public bool IsAnniversary { get; }
    //    public DateTimeOffset ToDateTimeOffset() => dateTimeOffset;

    //    public override string ToString() => $"{nameof(Date)}(anniversary:{IsAnniversary}, dateTimeOffset:{dateTimeOffset})";

    //    public static bool operator ==(Date left, Date right) => left.dateTimeOffset == right.dateTimeOffset;
    //    public static bool operator !=(Date left, Date right) => left.dateTimeOffset != right.dateTimeOffset;
    //    public static bool operator  <(Date left, Date right) => left.dateTimeOffset  < right.dateTimeOffset;
    //    public static bool operator  >(Date left, Date right) => left.dateTimeOffset  > right.dateTimeOffset;
    //    public static bool operator <=(Date left, Date right) => left.dateTimeOffset <= right.dateTimeOffset;
    //    public static bool operator >=(Date left, Date right) => left.dateTimeOffset >= right.dateTimeOffset;

    //    public static bool operator ==(Date left, DateTimeOffset right) => left.dateTimeOffset == right;
    //    public static bool operator !=(Date left, DateTimeOffset right) => left.dateTimeOffset != right;
    //    public static bool operator  <(Date left, DateTimeOffset right) => left.dateTimeOffset  < right;
    //    public static bool operator  >(Date left, DateTimeOffset right) => left.dateTimeOffset  > right;
    //    public static bool operator <=(Date left, DateTimeOffset right) => left.dateTimeOffset <= right;
    //    public static bool operator >=(Date left, DateTimeOffset right) => left.dateTimeOffset >= right;

    //    public static bool operator ==(DateTimeOffset left, Date right) => left == right.dateTimeOffset;
    //    public static bool operator !=(DateTimeOffset left, Date right) => left != right.dateTimeOffset;
    //    public static bool operator  <(DateTimeOffset left, Date right) => left  < right.dateTimeOffset;
    //    public static bool operator  >(DateTimeOffset left, Date right) => left  > right.dateTimeOffset;
    //    public static bool operator <=(DateTimeOffset left, Date right) => left <= right.dateTimeOffset;
    //    public static bool operator >=(DateTimeOffset left, Date right) => left >= right.dateTimeOffset;

    //    public static Date Min(Date a, Date b)
    //    {
    //        return a < b ? a : b;
    //    }
    //}
}

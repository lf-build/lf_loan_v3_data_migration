﻿using LendFoundry.Loans;
using LendFoundry.Migration.Loans.V3.Principal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class InterestCalculator
    {
        public InterestCalculator(PrincipalBalanceCalculator principalBalanceCalculator, InterestAccrualDurationCalculator durationCalculator, LoanCalendarFactory calendarFactory)
        {
            PrincipalBalanceCalculator = principalBalanceCalculator;
            DurationCalculator = durationCalculator;
            CalendarFactory = calendarFactory;
        }

        private LoanCalendarFactory CalendarFactory { get; }
        private InterestAccrualDurationCalculator DurationCalculator { get; }
        private PrincipalBalanceCalculator PrincipalBalanceCalculator { get; }
        
        public double CalculateInterest(ILoanTerms loanTerms, IEnumerable<PrincipalBalance> principalBalances, Date end)
        {
            if (loanTerms == null) throw new ArgumentNullException(nameof(loanTerms));
            if (principalBalances == null) throw new ArgumentNullException(nameof(principalBalances));
            if (!principalBalances.Any()) throw new ArgumentException("At least one principal balance is required", nameof(principalBalances));

            var durations = DurationCalculator.Calculate(loanTerms, principalBalances, end);
            var rate = loanTerms.Rate / 100.0;
            var interest = 0.0;

            foreach (var duration in durations)
            {
                var dailyRate = rate / duration.DaysInTheYear;
                interest += Math.Round(dailyRate * duration.Principal * duration.Days, 2, MidpointRounding.AwayFromZero);
            }

            return Math.Round(interest, 2, MidpointRounding.AwayFromZero);
        }

        public double GetInterestOwedFor(IInstallment installment, IPaymentSchedule schedule, ILoan loan)
        {
            return GetInterestOwedOn(installment.InterestPaymentDate, schedule, loan);
        }

        public double GetInterestOwedOn(DateTimeOffset endOfInterestAccrual, IPaymentSchedule schedule, ILoan loan)
        {
            return GetInterestOwedOn(new Date(endOfInterestAccrual), schedule, loan);
        }

        public double GetInterestOwedOn(Date endOfInterestAccrual, IPaymentSchedule schedule, ILoan loan)
        {
            var principalBalances = PrincipalBalanceCalculator.SinceLastInterestPaymentBefore(endOfInterestAccrual.Time, schedule, loan);

            var interestAccrued = CalculateInterest(loan.Terms, principalBalances, endOfInterestAccrual);

            var interestBalance = GetInterestBalanceOn(endOfInterestAccrual, schedule);

            return Math.Round(interestAccrued + interestBalance, 2, MidpointRounding.AwayFromZero);
        }

        private double GetInterestBalanceOn(Date date, IPaymentSchedule schedule)
        {
            var previousInstallment = schedule.Installments
                .Where(i => i.InterestPaymentDate < date)
                .LastOrDefault();

            return previousInstallment?.InterestBalance ?? 0.0;
        }
    }
}

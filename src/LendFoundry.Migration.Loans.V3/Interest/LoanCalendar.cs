﻿using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class LoanCalendar
    {
        private IList<IDateInfo> dates;

        public LoanCalendar(int initialTerm, DateTime noteDate, ICalendarService calendarService, ITenantTime tenantTime)
        {
            InitialTerm = initialTerm;
            CurrentTerm = initialTerm;
            NoteDate = noteDate;
            CalendarService = calendarService;
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        private ICalendarService CalendarService { get; }

        private DateTime NoteDate { get; }

        private int InitialTerm { get; }

        private int CurrentTerm { get; set; }

        private IList<IDateInfo> Dates => dates ?? (dates = GetDates());

        public DateTimeOffset GetDueDate(int period)
        {
            var info = GetDateInfo(period);

            var date = info.Type == DateType.Business ?
                info.Date :
                info.NextBusinessDay;

            return TenantTime.Convert(date);
        }

        private IList<IDateInfo> GetDates()
        {
            // For now the amortization service only works
            // with monthly payments. This is the place we
            // need to look at when other payment frequencies
            // must be supported.
            var periodicity = Periodicity.Monthly;

            var termPlusNoteDate = CurrentTerm + 1;
            var datesIncludingNoteDate = CalendarService.GetPeriodicDates(NoteDate, termPlusNoteDate, periodicity);
            return datesIncludingNoteDate
                .OrderBy(i => i.Date)
                .ToList();
        }

        public DateTimeOffset GetAnniversaryDate(int period)
        {
            var info = GetDateInfo(period);
            return TenantTime.Convert(info.Date);
        }

        private IDateInfo GetDateInfo(int period)
        {
            if (period > CurrentTerm)
                IncreaseTermBy(period - CurrentTerm);

            return Dates[period];
        }

        public DateTimeOffset GetAnniversaryDate(DateTimeOffset dueDate)
        {
            var info = Dates
                .Where(candidate => candidate.Type == DateType.Business ? candidate.Date == dueDate : candidate.NextBusinessDay == dueDate)
                .SingleOrDefault();

            if (info == null)
                throw new ArgumentException($"Anniversary date not found for due date {dueDate}");

            return info.Date;
        }

        public int NextPeriodAfter(DateTimeOffset date)
        {
            var dateInfo = GetNextDateInfo(date);
            return Dates.IndexOf(dateInfo);
        }

        public DateTimeOffset GetNextAnniversaryDateAfter(DateTimeOffset date)
        {
            return GetNextDateInfo(date).Date;
        }

        public DateTimeOffset GetPreviousAnniversaryDateUpTo(DateTimeOffset date)
        {
            return GetLastDateInfoUpTo(date).Date;
        }

        private IDateInfo GetLastDateInfoUpTo(DateTimeOffset date)
        {
            if (date < TenantTime.FromDate(NoteDate))
                throw new ArgumentException("Date must be past note date");

            return FindDateInfo(index =>
            {
                var info = Dates[index];

                if (info.Date == date)
                    return info;

                if (info.Date > date)
                    return Dates[index - 1];

                return null;
            });
        }

        private IDateInfo GetNextDateInfo(DateTimeOffset date)
        {
            if (date < TenantTime.FromDate(NoteDate))
                throw new ArgumentException("Date must be past note date");

            return FindDateInfo(index =>
            {
                var info = Dates[index];

                if (info.Date > date)
                    return info;

                return null;
            });
        }

        private IDateInfo FindDateInfo(Func<int, IDateInfo> fn)
        {
            return FindDateInfo(0, fn);
        }

        private IDateInfo FindDateInfo(int startIndex, Func<int, IDateInfo> fn)
        {
            int index = startIndex;

            for (; index < Dates.Count; index++)
            {
                var info = fn(index);
                if (info != null)
                    return info;
            }

            IncreaseTerm();

            return FindDateInfo(index, fn);
        }

        private void IncreaseTerm()
        {
            IncreaseTermBy(Math.Max(1, InitialTerm / 2));
        }

        private void IncreaseTermBy(int additionalLength)
        {
            CurrentTerm += additionalLength;
            dates = null;
        }
    }
}

﻿using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class LoanCalendarFactory
    {
        public LoanCalendarFactory(ICalendarService calendarService, ITenantTime tenantTime)
        {
            CalendarService = calendarService;
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        private ICalendarService CalendarService { get; }
        
        public LoanCalendar Create(int term, DateTime noteDate)
        {
            return new LoanCalendar(term, noteDate, CalendarService, TenantTime);
        }
    }
}

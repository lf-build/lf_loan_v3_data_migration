﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans;
using LendFoundry.Migration.Loans.V3.Principal;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LendFoundry.Migration.Loans.V3.Interest
{
    public class InterestAccrualDurationCalculator
    {
        public InterestAccrualDurationCalculator(ITenantTime tenantTime)
        {
            CalendarType = new Finra360CalendarType();
            TenantTime = tenantTime;
        }

        private Finra360CalendarType CalendarType { get; }
        private ITenantTime TenantTime { get; }
        
        public IEnumerable<InterestAccrualDuration> Calculate(ILoanTerms loanTerms, IEnumerable<PrincipalBalance> principalBalances, Date endOfAccrual)
        {
            /*
                We cannot simply calculate the days between one period and the next period
                because, when using the 360-day calendar, the total number of days would be
                off in cases like this:

                    From Dec/15 to Dec/31 there are 16 days
                    From Dec/31 to Jan/15 there are 15 days

                Which adds up to a total of 31 days. However, according to the 360-day calendar,
                from Dec/15 to Jan/15 there are 30 days. To compensate for this error, we calculate
                the no. of days between the beginning of the first period to the end of the current
                period and subtract the total no. of days up to the current period.

                This approach works for both 360-day and actual calendars.
            */

            var balances = new List<PrincipalBalance>(principalBalances);
            var durations = new List<InterestAccrualDuration>();

            var firstDate = principalBalances.First().Date;
            var totalDays = 0;

            for (int index = 0; index < balances.Count; index++)
            {
                var current = balances[index];
                var next = index < balances.Count - 1 ? balances[index + 1] : null;
                var start = current.Date;
                var end = next?.Date ?? endOfAccrual;

                foreach (var interval in SplitIntervalByCalendarYears(start, end))
                {
                    var daysFromFirstDateToNextDate = CalendarType.DaysBetween(firstDate, interval.End, loanTerms.OriginationDate);
                    var days = daysFromFirstDateToNextDate - totalDays;

                    totalDays += days;

                    durations.Add(new InterestAccrualDuration
                    {
                        Days = days,
                        DaysInTheYear = CalendarType.GetDaysInTheYear(interval.Start.Time.Year),
                        Principal = current.Amount
                    });
                }
            }

            return durations;
        }

        private IEnumerable<Interval> SplitIntervalByCalendarYears(Date start, Date end)
        {
            if (DaysInTheYear(start) == DaysInTheYear(end))
            {
                yield return new Interval(start, end);
            }
            else
            {
                Date nextStart = start;
                Date nextEnd;
                do
                {
                    nextEnd = Min(BeginningOfNextYear(nextStart), end);
                    yield return new Interval(nextStart, nextEnd);
                    nextStart = nextEnd;
                }
                while (nextEnd < end);
            }
        }

        private static Date Min(Date a, Date b)
        {
            if (a < b) return a;
            return b;
        }

        private int DaysInTheYear(Date date)
        {
            return CalendarType.GetDaysInTheYear(date.Time.Year);
        }

        private Date BeginningOfNextYear(Date date)
        {
            var dateTimeOffset = TenantTime.Create(date.Time.Year + 1, 1, 1);
            return new Date(dateTimeOffset);
        }

        private struct Interval
        {
            public Interval(Date start, Date end)
            {
                Start = start;
                End = end;
            }

            public Date Start { get; }
            public Date End { get; }
        }
    }
}

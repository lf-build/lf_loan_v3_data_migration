﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3
{
    public class MigrationErrors
    {
        public MigrationErrors(Dictionary<string, IEnumerable<string>> errors, MigrationConfiguration configuration)
        {
            Errors = WithoutKnownIssues(errors, configuration.KnownIssues);
            KnownIssues = GetKnownIssuesWithErrors(errors, configuration.KnownIssues);
        }

        private List<KnownIssue> GetKnownIssuesWithErrors(Dictionary<string, IEnumerable<string>> errors, KnownIssue[] knownIssues)
        {
            var result = new List<KnownIssue>();

            foreach(var knownIssue in knownIssues)
            {
                var referenceNumber = knownIssue.ReferenceNumber;
                if (errors.ContainsKey(referenceNumber))
                {
                    var loanErrors = errors[referenceNumber];
                    result.Add(new KnownIssue
                    {
                        ReferenceNumber = referenceNumber,
                        Description = knownIssue.Description,
                        Errors = loanErrors
                    });
                }
            }

            return result;
        }

        private Dictionary<string, IEnumerable<string>> WithoutKnownIssues(Dictionary<string, IEnumerable<string>> errors, KnownIssue[] knownIssues)
        {
            var loansWithKnownIssues = new HashSet<string>(knownIssues.Select(ki => ki.ReferenceNumber));
            var result = new Dictionary<string, IEnumerable<string>>();

            foreach (var entry in errors.Where(e => !loansWithKnownIssues.Contains(e.Key)))
            {
                result.Add(entry.Key, entry.Value);
            }

            return result;
        }

        public Dictionary<string, IEnumerable<string>> Errors { get; }
        public List<KnownIssue> KnownIssues { get; }
    }
}

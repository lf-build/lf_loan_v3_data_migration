﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Migration.Loans.V3
{
    internal static class Settings
    {
        public static string ServiceName { get; } = "migration-loans-v3";
        public static ServiceSettings CalendarService { get; } = new ServiceSettings("MIGRATION_CALENDAR_HOST", "calendar", "MIGRATION_CALENDAR_PORT");
        public static ServiceSettings ConfigurationService { get; } = new ServiceSettings("MIGRATION_CONFIGURATION_HOST", "configuration", "MIGRATION_CONFIGURATION_PORT");
        public static ServiceSettings TenantService { get; } = new ServiceSettings("MIGRATION_TENANT_HOST", "tenant", "MIGRATION_TENANT_PORT");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings("MIGRATION_MONGO_CONNECTIONSTRING", "mongodb://mongo:27017", "MIGRATION_MONGO_DATABASE", "loans");
    }
}
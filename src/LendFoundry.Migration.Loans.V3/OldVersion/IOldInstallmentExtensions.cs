﻿using System;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public static class OldInstallmentExtensions
    {
        public static DateTimeOffset PrincipalPaymentDate(this IOldInstallment i)
        {
            return IsEarlyPayment(i) || IsLatePayment(i)
                ? i.PaymentDate.Value
                : i.AnniversaryDate.HasValue ? i.AnniversaryDate.Value : (i.PaymentDate ?? i.DueDate).Value;
        }

        public static bool IsLatePayment(this IOldInstallment i)
        {
            return 
                i.PaymentDate.HasValue &&
                i.PaymentDate > i.DueDate.Value.AddDays(GracePeriod.InDays);
        }

        public static bool IsEarlyPayment(this IOldInstallment i)
        {
            return 
                i.PaymentDate.HasValue && i.AnniversaryDate.HasValue &&
                i.PaymentDate < i.AnniversaryDate;
        }
    }
}

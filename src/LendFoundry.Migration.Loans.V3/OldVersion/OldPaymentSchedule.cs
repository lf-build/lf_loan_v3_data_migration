﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public class OldPaymentSchedule : Aggregate, IOldPaymentSchedule
    {
        private IEnumerable<IOldInstallment> installments;
        public string LoanReferenceNumber { get; set; }

        public IEnumerable<IOldInstallment> Installments
        {
            get
            {
                if (installments == null)
                    return new List<IOldInstallment>();

                return installments
                    .OrderBy(i => i, InstallmentComparer.Instance)
                    //.OrderBy(i => SortingDate(i))
                    //.ThenBy(i => (i.AnniversaryDate ?? i.PaymentDate ?? i.DueDate).Value)
                    .ToList();
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));

                installments = value;
            }
        }

        //private DateTimeOffset SortingDate(IOldInstallment i)
        //{
        //    if (i.Type == OldInstallmentType.Scheduled)
        //    {
        //        if (i.PaymentDate == null)
        //        {
        //            return i.AnniversaryDate.Value;
        //        }

        //        if (i.IsEarlyPayment() || i.IsLatePayment())
        //            return i.PaymentDate.Value;
        //        else
        //            return i.AnniversaryDate.Value;
        //    }

        //    return (i.PaymentDate ?? i.DueDate).Value;
        //}

        private static DateTimeOffset? GetEffectiveInstallmentDate(IOldInstallment installment)
        {
            if (installment.PaymentDate.HasValue && installment.DueDate.HasValue)
                return installment.PaymentDate.Value > installment.DueDate.Value
                    ? installment.PaymentDate.Value
                    : installment.DueDate.Value;

            if (installment.PaymentDate.HasValue)
                return installment.PaymentDate.Value;

            return installment.DueDate.Value;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public class InstallmentComparer : IComparer<IOldInstallment>
    {
        public static readonly InstallmentComparer Instance = new InstallmentComparer();

        public int Compare(IOldInstallment i1, IOldInstallment i2)
        {
            var result = i1.PrincipalPaymentDate().CompareTo(i2.PrincipalPaymentDate());
            if (result != 0)
                return result;


            result = i1.Type.CompareTo(i2.Type);
            if (result != 0)
                return result;


            result = CompareAnniversaryDates(i1, i2);
            if (result != 0)
                return result;

            return i2.EndingBalance.CompareTo(i1.EndingBalance);
        }

        private int CompareAnniversaryDates(IOldInstallment i1, IOldInstallment i2)
        {
            var a1 = i1.AnniversaryDate;
            var a2 = i2.AnniversaryDate;
            if (a1.HasValue && a2.HasValue) return a1.Value.CompareTo(a2.Value);
            if (a1.HasValue && !a2.HasValue) return -1;
            if (!a1.HasValue && a2.HasValue) return 1;
            return 0;
        }
    }
}

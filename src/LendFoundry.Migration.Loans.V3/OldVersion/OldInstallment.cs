﻿using System;
using PaymentMethod = LendFoundry.Loans.PaymentMethod;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public class OldInstallment : IOldInstallment
    {
        public PaymentMethod PaymentMethod { get; set; }
        public DateTimeOffset? AnniversaryDate { get; set; }
        public DateTimeOffset? DueDate { get; set; }
        public DateTimeOffset? PaymentDate { get; set; }
        public string PaymentId { get; set; }
        public double OpeningBalance { get; set; }
        public double PaymentAmount { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double EndingBalance { get; set; }
        public OldInstallmentStatus Status { get; set; }
        public OldInstallmentType Type { get; set; }
    }
}

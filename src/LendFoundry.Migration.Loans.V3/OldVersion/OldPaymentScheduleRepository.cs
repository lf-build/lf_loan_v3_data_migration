﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public class OldPaymentScheduleRepository : MongoRepository<IOldPaymentSchedule, OldPaymentSchedule>
    {
        static OldPaymentScheduleRepository()
        {
            BsonClassMap.RegisterClassMap<OldPaymentSchedule>(map =>
            {
                map.AutoMap();

                var type = typeof(OldPaymentSchedule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IOldInstallment, OldInstallment>());

            BsonClassMap.RegisterClassMap<OldInstallment>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.PaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.AnniversaryDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(OldInstallment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public OldPaymentScheduleRepository(IMongoConfiguration configuration, ITenantService tenantService) :
                base(tenantService, configuration, "schedule_old")
        {
            CreateIndexIfNotExists
            (
               indexName: "loan-reference-number",
               index: Builders<IOldPaymentSchedule>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
            );
        }

        public List<string> GetRefNums()
        {
            return Query.Select(i => i.LoanReferenceNumber).ToList();
        }

        public IOldPaymentSchedule GetSchedule(string loanReferenceNumber)
        {
            return Query
                .Where(i => i.LoanReferenceNumber == loanReferenceNumber)
                .First();
        }
    }
}

﻿using System;
using PaymentMethod = LendFoundry.Loans.PaymentMethod;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public interface IOldInstallment
    {
        PaymentMethod PaymentMethod { get; set; }
        DateTimeOffset? AnniversaryDate { get; set; }
        DateTimeOffset? DueDate { get; set; }
        DateTimeOffset? PaymentDate { get; set; }
        string PaymentId { get; set; }
        double OpeningBalance { get; set; }
        double PaymentAmount { get; set; }
        double Principal { get; set; }
        double Interest { get; set; }
        double EndingBalance { get; set; }
        OldInstallmentStatus Status { get; set; }
        OldInstallmentType Type { get; set; }
    }

    public enum OldInstallmentType
    {
        Scheduled,
        Additional,
        Payoff,
        ChargeOff
    }

    public enum OldInstallmentStatus
    {
        Scheduled,
        Pending,
        Completed
    }
}
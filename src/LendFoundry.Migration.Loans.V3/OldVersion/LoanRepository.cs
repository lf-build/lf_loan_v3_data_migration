﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Linq;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public class LoanRepository : MongoRepository<ILoan, Loan>
    {
        static LoanRepository()
        {
            BsonClassMap.RegisterClassMap<Loan>(map =>
            {
                map.AutoMap();

                //Borrowers are stored separately in their own repository
                map.UnmapProperty(loan => loan.Borrowers);

                var type = typeof(Loan);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanTerms>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ApplicationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.FundedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.OriginationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(LoanTerms);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanInvestor>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.LoanPurchaseDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(LoanInvestor);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Score>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.InitialDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.UpdatedDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(Score);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<BankAccount>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.EffectiveDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.AccountType).SetSerializer(new EnumSerializer<BankAccountType>(BsonType.String));

                var type = typeof(BankAccount);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanStatus>(map =>
            {                
                map.MapMember(m => m.Code);
                map.MapMember(m => m.Name);
                map.MapMember(m => m.StartDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.EndDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));                                

                var type = typeof(LoanStatus);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanTerms, LoanTerms>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFee, Fee>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanInvestor, LoanInvestor>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IScore, Score>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IBankAccount, BankAccount>());
        }

        public LoanRepository(IMongoConfiguration configuration, ITenantService tenantService) : base(tenantService, configuration, "loans")
        {
            CreateIndexIfNotExists
            (
               indexName: "reference-number",
               index: Builders<ILoan>.IndexKeys.Ascending(i => i.ReferenceNumber)
            );

            CreateIndexIfNotExists
            (
               indexName: "status-code",
               index: Builders<ILoan>.IndexKeys.Ascending(i => i.Status.Code)
            );
        }

        public new ILoan Get(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException("Loan Reference Number must to be informed.");

            return Query.FirstOrDefault(q => q.ReferenceNumber == loanReferenceNumber);
        }
    }
}

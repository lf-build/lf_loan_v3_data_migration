﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Migration.Loans.V3.OldVersion
{
    public interface IOldPaymentSchedule: IAggregate
    {
        string LoanReferenceNumber { get; set; }
        IEnumerable<IOldInstallment> Installments { get; set; }
    }
}

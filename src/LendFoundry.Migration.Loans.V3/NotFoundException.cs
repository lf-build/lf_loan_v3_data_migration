﻿using System;

namespace LendFoundry.Migration.Loans.V3
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string msg) : base(msg)
        {
        }
    }
}

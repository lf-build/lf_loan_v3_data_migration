﻿using System.Collections.Generic;

namespace LendFoundry.Migration.Loans.V3
{
    public class LoanErrors
    {
        public string ReferenceNumber { get; set; }
        public List<string> Errors { get; set; }
    }
}

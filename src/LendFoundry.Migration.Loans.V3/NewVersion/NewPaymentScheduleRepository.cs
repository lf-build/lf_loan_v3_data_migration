﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;

namespace LendFoundry.Migration.Loans.V3.NewVersion
{
    public class NewPaymentScheduleRepository : MongoRepository<IPaymentSchedule, PaymentSchedule>
    {
        static NewPaymentScheduleRepository()
        {
            BsonClassMap.RegisterClassMap<PaymentSchedule>(map =>
            {
                map.AutoMap();

                var type = typeof(PaymentSchedule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IInstallment, Installment>());

            BsonClassMap.RegisterClassMap<Installment>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.PaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.AnniversaryDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.GraceDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.PaymentMethod).SetSerializer(new EnumSerializer<PaymentMethod>(BsonType.String));
                map.MapMember(p => p.Status).SetSerializer(new EnumSerializer<InstallmentStatus>(BsonType.String));
                map.MapMember(p => p.Type).SetSerializer(new EnumSerializer<InstallmentType>(BsonType.String));

                // Read-only properties:
                map.MapMember(i => i.Expected);
                map.MapMember(i => i.Actual);
                map.MapMember(i => i.EndingBalance);
                map.MapMember(i => i.IsPaid);
                map.MapMember(i => i.IsEarlyPayment);
                map.MapMember(i => i.IsLatePayment);
                map.MapMember(i => i.InterestBalance);

                // Transient properties:
                map.UnmapMember(i => i.InterestPaymentDate);
                map.UnmapMember(i => i.PrincipalPaymentDate);
                map.UnmapMember(i => i.IsOpen);
                map.UnmapMember(i => i.IsInterestOwed);
                map.UnmapMember(i => i.CanPaymentBeAllocated);

                var type = typeof(Installment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Paydown>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Amount);
                map.MapMember(p => p.Interest);
                map.MapMember(p => p.Principal);

                var type = typeof(Paydown);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Date>(map =>
            {
                map.AutoMap();
                map.MapMember(d => d.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(d => d.IsAnniversary);

                var type = typeof(Date);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public NewPaymentScheduleRepository(IMongoConfiguration configuration, ITenantService tenantService) :
                base(tenantService, configuration, "schedule")
        {
            CreateIndexIfNotExists
            (
               indexName: "loan-reference-number",
               index: Builders<IPaymentSchedule>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
            );
        }

        //static NewPaymentScheduleRepository()
        //{
        //    BsonClassMap.RegisterClassMap<PaymentSchedule>(map =>
        //    {
        //        map.AutoMap();

        //        var type = typeof(PaymentSchedule);
        //        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        //        map.SetIsRootClass(true);
        //    });

        //    BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IInstallment, Installment>());

        //    BsonClassMap.RegisterClassMap<Installment>(map =>
        //    {
        //        map.AutoMap();
        //        map.MapMember(m => m.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
        //        map.MapMember(m => m.PaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
        //        map.MapMember(m => m.AnniversaryDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

        //        var type = typeof(Installment);
        //        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        //    });
        //}

        //public NewPaymentScheduleRepository(IMongoConfiguration configuration, ITenantService tenantService) : base(tenantService, configuration, "schedule")
        //{
        //    CreateIndexIfNotExists
        //    (
        //       indexName: "loan-reference-number",
        //       index: Builders<IPaymentSchedule>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
        //    );
        //}

        public void Upsert(PaymentSchedule schedule)
        {
            var query = Query.Where(s => s.LoanReferenceNumber == schedule.LoanReferenceNumber);

            if (query.Any())
            {
                var existing = query.Select(s => new { s.Id, s.TenantId }).Single();
                schedule.Id = existing.Id;
                schedule.TenantId = existing.TenantId;
                Update(schedule);
            }
            else
            {
                Add(schedule);
            }
        }

        public IPaymentSchedule GetSchedule(string refnum)
        {
            return Query.Where(s => s.LoanReferenceNumber == refnum).SingleOrDefault();
        }
    }
}
